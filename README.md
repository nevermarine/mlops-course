## MLOps-course
This is a repository for the [ods.ai](https://ods.ai/) course, [MLOps и production в DS исследованиях 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).
The up-to-date information is in [CONTRIBUTING.md](CONTRIBUTING.md) for now.

### Building
Run the following command in your terminal:
```bash
docker build . -t hello-world
```
Then you can run the container like this:
```bash
docker run hello-world
```
