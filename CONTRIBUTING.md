# Contributing Guidelines


## Setting Up Your Development Environment

To ensure that all contributions adhere to our code quality and formatting standards, we use several tools, including `pre-commit`, `Black`, and `Flake8`.

### Pre-requisites

- Python 3.8 or newer
- [Git](https://git-scm.com/)
- [poetry](https://python-poetry.org/)

### Installing Dependencies
   ```sh
   git clone https://gitlab.com/nevermarine/mlops-course
   cd your-project
   poetry install
   ```
You should also install pre-commit on your machine.

## Branches
All branches should have this structure: `<tag>/<description>`. Available tags:
- wa
- bugfix
- test
- debug
- feature
- refactor

For example:
- `wa/change-conn-str-template`
- `refactor/func-for-deglaze-subroutine`


## Tags and releases
All tags and releases should follow [the semantic versioning](https://semver.org/) model.
