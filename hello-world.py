def main():
    message = "Hello, World!"  # Space around the operator
    for char in message:
        print(char, end="")
    print()  # Move to the next line


if __name__ == "__main__":
    main()
